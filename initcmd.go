package main

import "os"
import "flag"
import "path/filepath"
import . "fmt"

var initFlagSet = flag.NewFlagSet("initFlagSet", flag.ExitOnError)

var initRootDir = initFlagSet.String("root-dir", "", "Specify alternate root directory")


func initcmd(args []string) {

	initFlagSet.Parse(args)
	args = initFlagSet.Args()

	var root string
	if *initRootDir == "" {
		var wd, err = os.Getwd();
		if err != nil {
			Fprint(os.Stderr, err)
			os.Exit(1)
		}
		root = wd
	} else {
		var wd, err = filepath.Abs(*initRootDir)
		if err != nil {
			Fprint(os.Stderr, err)
			os.Exit(1)
		}
		root = wd
	}

	var finfo, err = os.Stat(root)
	if err != nil {
		Fprintln(os.Stderr, "Root not found: "+root)
		os.Exit(1)
	} else if !finfo.IsDir() {
		Fprintln(os.Stderr, "Root not a direcory")
		os.Exit(1)
	}

	root = filepath.Join(root, ".tondef")

	finfo, err = os.Stat(root)
	if err == nil {
		Fprintln(os.Stderr, "Root is already initialized")
		os.Exit(1)
	}

	err = os.Mkdir(root, os.ModePerm)
	if err != nil {
		Fprintln(os.Stderr, "Error while creating root:", err)
		os.Exit(1)
	}

	os.Exit(0)
}