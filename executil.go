package main

import "os/exec"
import "container/list"


type ExecCmdReq struct {
	Name string
	Args []string
	Ch chan *exec.Cmd
}

func (this *ExecCmdReq) Command() {
	this.Ch <- exec.Command(this.Name, this.Args...)
}


var execCmdReqChan = make(chan *ExecCmdReq)

var execCmdRtnChan = make(chan *exec.Cmd)



func StartExecCmdManager(max int) {
	if max <= 0 {
		panic("Maximum number of exec commands must be greater than zero.")
	}
	go srvExecCmd(max)
}


// reqExecCmd requests a new execacutable command.
func RequestExecCmd(name string, args ...string) (*exec.Cmd) {
	var ch = make(chan *exec.Cmd)
	execCmdReqChan <- &ExecCmdReq{ name, args, ch }
	return <-ch
}

func ReturnExecCmd(cmd *exec.Cmd) {
	execCmdRtnChan <- cmd
}

// srvExecCmd recieves command requests and responds with a command
func srvExecCmd(max int) {

	var used = 0
	var queue = list.New()

	for {
		select {
		case req := <- execCmdReqChan:
			if used < max {
				req.Command()
				used++
			} else {
				queue.PushBack(req)
			}

		case cmd := <- execCmdRtnChan:
			if cmd.ProcessState != nil && !cmd.ProcessState.Exited() {
				cmd.Process.Kill()
			}
			used--
			if queue.Len() > 0 {
				var next = queue.Front()
				queue.Remove(next)
				var req = next.Value.(*ExecCmdReq)
				req.Command()
				used++
			}
		}
	}
}
