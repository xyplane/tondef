
package main


import "os"
import "flag"
import "errors"
import "path/filepath"
import . "fmt"
import . "strings"


var mainFlagSet = flag.NewFlagSet("mainFlagSet", flag.ContinueOnError)

var mainMaxExecCmds = mainFlagSet.Int("P", 1, "Maximum number of child processes" )

var mainVerbose = mainFlagSet.Bool("v", false, "Display more messages during processing" )

func main() {
	mainWithArgs(os.Args[1:])
}

func mainWithArgs(args []string) {

	mainFlagSet.Parse(args)
	args = mainFlagSet.Args()

	if *mainMaxExecCmds <= 0 {
		Fprintln(os.Stderr, "Maximum number of child processes must be greater than zero")
		*mainMaxExecCmds = 1
	}
	StartExecCmdManager(*mainMaxExecCmds)
	
	if len(args) == 0 {
		Fprintln(os.Stderr, "Enter a valid command")
		help(args)
	}

	var cmd = ToLower(TrimSpace(args[0]))

	switch  cmd {
	case "init":
		initcmd(args[1:])
	case "info":
		infocmd(args[1:])
	case "add":
		addcmd(args[1:])
	case "convert":
		convcmd(args[1:])
	case "help":
		help(args[1:])
	default:
		help(args[1:])
	}
}


func help(args []string) {
	Fprintln(os.Stderr, "")
	Fprintln(os.Stderr, "Commands:")
	Fprintln(os.Stderr, "  init: Initialize a music collection")
	Fprintln(os.Stderr, "  info: Print song information")
	Fprintln(os.Stderr, "  add: Add music to collection")
	Fprintln(os.Stderr, "  help: Display help information")
	Fprintln(os.Stderr, "")
	Fprintln(os.Stderr, "Options:")
	mainFlagSet.PrintDefaults()
	Fprintln(os.Stderr, "")
	os.Exit(0)
}


 func findRootDirectory(root string) (string, error) {
	
	if root == "" {
		var wd, err = os.Getwd()
		if err != nil {
			return "", err
		}

		for {
			var td = filepath.Join(wd, ".tondef")
			var finfo, ferr = os.Stat(td)
			if ferr == nil && finfo.IsDir() {
				return wd, nil
			}

			var parent = filepath.Dir(wd)
			if(parent == wd) {
				break
			}

			wd = parent
		}
	} else {
		var ab, err = filepath.Abs(root)
		if err != nil {
			return "", err
		}
		
		var td = filepath.Join(ab, ".tondef")
		var finfo, ferr = os.Stat(td)
		if ferr == nil && finfo.IsDir() {
			return ab, nil
		}
	}

	return "", errors.New("Root directory not found")
}
