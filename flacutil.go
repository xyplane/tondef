package main

//import "os"
import "path/filepath"

var FLAC_EXT = ".flac"


func IsFlacExt(ext string) bool {
	return ext == FLAC_EXT
}

func GetFlacExt() string {
	return FLAC_EXT
}

func ConvToFlacAll(paths, fileTypes []string) {

	

}

func ConvToFlac(path, fileType string) (string, error) {

	var srcFileExt = filepath.Ext(path)
	var destFilePath = path[:len(path)-len(srcFileExt)]+FLAC_EXT

	//var stat, statErr = os.Stat(destFilePath)

	var cmd = RequestExecCmd("avconv", "-i", path, destFilePath)
	defer ReturnExecCmd(cmd)

	// var stdout, oerr = cmd.StdoutPipe()
	// if oerr != nil {
	// 	return nil, oerr
	// }

	// var err = cmd.Run()
	// if err != nil {
	// 	return destFilePath, err
	// }

	return destFilePath, cmd.Run()
}
