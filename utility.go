package main

import "os"
import "sync"
import "errors"
import "path/filepath"
import . "fmt"


type MusicInfo struct {
	Type string
	Title string
	Album string
	Artist string
	DiskNumber int
	DiskTotal int
	TrackNumber int
	TrackTotal int
	Genre string
	Composer string
	Date int
	WavChkSum string
}

func (this *MusicInfo) String() string {
	var str string
	str += Sprintln("Type:", this.Type)
	str += Sprintln("Artist:", this.Artist)
	str += Sprintln("Composer:", this.Composer)
	str += Sprintln("Title:", this.Title)
	str += Sprintln("Album:", this.Album)
	str += Sprintln("Date:", this.Date)
	str += Sprintln("Track:", this.TrackNumber, " of ", this.TrackTotal)
	str += Sprintln("Disk:", this.DiskNumber, " of ", this.DiskTotal)
	str += Sprintln("Genre:", this.Genre)
	str += Sprintln("WavChecksum:", this.WavChkSum)
	return str
}


var musicExtChecks = map[string]func(string) bool {
	"m4a":IsM4aExt }

var musicExtGetters = map[string]func() string {
	"m4a":GetM4aExt }

var musicFileChecks = map[string]func(*os.File) (bool, error) {
	"m4a":IsM4aFile }

var musicInfoGetters = map[string]func(string) (*MusicInfo, error) {
	"m4a":GetM4aInfo }


func IsMusicPathAll(paths []string) ([]string, []error) {
	var wg sync.WaitGroup
	var results = make([]string, len(paths))
	var rerrors = make([]error, len(paths))
	for idx, path := range paths {
		wg.Add(1)
		go func(idx int, path string) {
			defer wg.Done()
			results[idx], rerrors[idx] = IsMusicPath(path)
		}(idx, path)
	}
	wg.Wait()
	return results, rerrors
}


func IsMusicPath(path string) (string, error) {
	info, serr := os.Stat(path)
	if serr != nil {
		if os.IsNotExist(serr) {
			return "", errors.New("File Not Found: " + path)
		}
		return "", serr
	}
	if info.IsDir() {
		return "", errors.New("Directory found: " + path)
	}
	file, oerr := os.Open(path)
	if oerr != nil {
		return "", oerr
	}
	defer file.Close()
	return IsMusicFile(file)
}



func IsMusicFile(file *os.File) (string, error) {
	
	var ext = filepath.Ext(file.Name())

	var fileType = ""

	for k, v := range musicExtChecks {
		if v(ext) {
			fileType = k
			break
		}
	}

	if v, ok := musicFileChecks[fileType]; ok {
		if ft, err := v(file); ft {
			return fileType, err
		}
	}

	for k, v := range musicFileChecks {
		if k == fileType {
			continue
		}
		if ft, err := v(file); ft {
			return k, err
		}
	}

	return "", nil
}


func GetMusicInfoAll(paths, fileTypes  []string) ([]*MusicInfo, []error) {
	var wg sync.WaitGroup
	var results = make([]*MusicInfo, len(paths))
	var rerrors = make([]error, len(paths))
	for idx, path := range paths {
		wg.Add(1)
		go func(idx int, path, fileType string) {
			defer wg.Done()
			results[idx], rerrors[idx] = GetMusicInfo(path, fileType)
		}(idx, path, fileTypes[idx])
	}
	wg.Wait()
	return results, rerrors
}

func GetMusicInfo(path, fileType string) (*MusicInfo, error) {
	var musicInfoGetter, ok = musicInfoGetters[fileType]
	if !ok {
		return nil, errors.New("Invalid file type specified: " + fileType)
	}
	return musicInfoGetter(path)
}

func GetMusicExt(fileType string) (string, error) {
	var getter, ok = musicExtGetters[fileType]
	if !ok {
		return "", errors.New("Invalid file type specified: " + fileType)
	}
	return getter(), nil
}

func Deduplicate(strs []string) ([]string, []int) {
	var unistrs = make([]string, 0, len(strs))
	var dupidxs = make([]int, 0)
	for idx, str := range strs {
		var unique = true
		for _, unistr := range unistrs {
			if str == unistr {
				unique = false
				break
			}
		}
		if unique {
			unistrs = append(unistrs, str)
		} else {
			dupidxs = append(dupidxs, idx)
		}
	}
	return unistrs, dupidxs
}

func NormalizePaths(paths []string) []string {
	var wg sync.WaitGroup
	var norms = make([]string, len(paths))
	for idx, path := range paths {
		wg.Add(1)
		go func(idx int, path string) {
			defer wg.Done()
			norms[idx] = NormalizePath(path)
		}(idx, path)
	}
	wg.Wait()
	return norms
}

func NormalizePath(path string) string  {
	var cln = filepath.Clean(path)
	var abs, err = filepath.Abs(cln)
	if err != nil {
		return cln
	}	
	return abs
}
