package main

import "os"
import "flag"
import . "fmt"

var infoFlagSet = flag.NewFlagSet("infoFlagSet", flag.ExitOnError)

var infoVerbose = infoFlagSet.Bool("v", false, "Display more messages during processing" )


func infocmd(args []string) {

	infoFlagSet.Parse(args)
	args = infoFlagSet.Args()

	var paths, dupls = Deduplicate(NormalizePaths(args))
	for _, dupl := range dupls {
		Fprintln(os.Stderr, "Duplicate file specified, ignoring:", args[dupl])
	}
	
	var exit = false

	var fileTypes, fterrs = IsMusicPathAll(paths)
	for idx, fileType := range fileTypes {
		if fileType == "" {
			Fprintln(os.Stderr, "Not a recognized music file:", paths[idx])
			if (*mainVerbose || *infoVerbose) && fterrs[idx] != nil {
				Fprintln(os.Stderr, "Not a recognized music file:", fterrs[idx])
			}
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var musicInfos, _ = GetMusicInfoAll(paths, fileTypes)
	for idx, musicInfo := range musicInfos {
		Fprintln(os.Stdout, "File:", paths[idx])
		if musicInfo != nil {
			Fprintln(os.Stdout, musicInfo.String())
		} else {
			Fprintln(os.Stdout, "No Music Information Found")
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	os.Exit(0)
}
