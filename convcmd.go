package main

import "os"
//import "io"
import "flag"
//import "sync"
//import "regexp"
//import "errors"
//import "strings"
//import "path/filepath"
import . "fmt"

var convFlagSet = flag.NewFlagSet("convFlagSet", flag.ExitOnError)

var convRootDir = convFlagSet.String("root-dir", "", "Specify alternate root directory")

var convVerbose = convFlagSet.Bool("v", false, "Display more messages during processing" )

var convFileType = convFlagSet.String("type", "flac", "Specify file type to convert")


func convcmd(args []string) {

	convFlagSet.Parse(args)
	args = convFlagSet.Args()

	// var root, err = findRootDirectory(*convRootDir)
	// if err != nil {
	// 	Fprintln(os.Stderr, "Root directory not found. Are you in a Collection?")
	// 	os.Exit(1)
	// }

	var paths, dupls = Deduplicate(NormalizePaths(args))
	for _, dupl := range dupls {
		Fprintln(os.Stderr, "Duplicate file specified, ignoring:", args[dupl])
	}

	var exit = false

	var fileTypes, fterrs = IsMusicPathAll(paths)
	for idx, fileType := range fileTypes {
		if fileType == "" {
			Fprintln(os.Stderr, "Not a recognized music file:", paths[idx])
			if (*mainVerbose || *addVerbose) && fterrs[idx] != nil {
				Fprintln(os.Stderr, "Not a recognized music file:", fterrs[idx])
			}
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var musicInfos, _ = GetMusicInfoAll(paths, fileTypes)
	for idx, musicInfo := range musicInfos {
		if musicInfo == nil {
			Fprintln(os.Stderr, "No Music Information Found:", paths[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}



}
