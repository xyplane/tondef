
package main

import "os"
import "io"
import "flag"
import "crypto"
import _ "crypto/md5"
import "hash"
import "sync"
import "bufio"
import "regexp"
import "errors"
import "strings"
import "path/filepath"
import "encoding/json"
import "encoding/hex"
import . "fmt"


var addFlagSet = flag.NewFlagSet("addFlagSet", flag.ExitOnError)

var addRootDir = addFlagSet.String("root-dir", "", "Specify alternate root directory")

var addVerbose = addFlagSet.Bool("v", false, "Display more messages during processing" )

var addMd5sumFile = addFlagSet.String("md5sum", "", "Obtain WAV checksums from a md5sum file" )

func addcmd(args []string) {

	addFlagSet.Parse(args)
	args = addFlagSet.Args()

	var root, err = findRootDirectory(*addRootDir)
	if err != nil {
		Fprintln(os.Stderr, "Root directory not found. Are you in a Collection?")
		os.Exit(1)
	}

	var paths, dupls = Deduplicate(NormalizePaths(args))
	for _, dupl := range dupls {
		Fprintln(os.Stderr, "Duplicate file specified, ignoring:", args[dupl])
	}

	var exit = false

	var fileTypes, fterrs = IsMusicPathAll(paths)
	for idx, fileType := range fileTypes {
		if fileType == "" {
			Fprintln(os.Stderr, "Not a recognized music file:", paths[idx])
			if (*mainVerbose || *addVerbose) && fterrs[idx] != nil {
				Fprintln(os.Stderr, "Not a recognized music file:", fterrs[idx])
			}
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var musicInfos, _ = GetMusicInfoAll(paths, fileTypes)
	for idx, musicInfo := range musicInfos {
		if musicInfo == nil {
			Fprintln(os.Stderr, "No Music Information Found:", paths[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	if *addMd5sumFile != "" {
		var md5sumFile, md5sumFileErr = os.Open(*addMd5sumFile)
		if md5sumFileErr != nil {
			Fprintln(os.Stderr, "Error reading md5sum file:", addMd5sumFile)
			os.Exit(1)
		}
		defer md5sumFile.Close()

		var md5records = bufio.NewScanner(md5sumFile)
		for md5records.Scan() {
			var md5record = strings.SplitN(md5records.Text(), " ", 2)
			if len(md5record) < 2 {
				continue
			}
			var md5RecHash = strings.TrimSpace(md5record[0])
			var md5RecFile = strings.TrimSpace(md5record[1])
			var md5RecFileExt = filepath.Ext(md5RecFile)
			var md5RecFilePrefix = md5RecFile[:len(md5RecFile)-len(md5RecFileExt)]
			for idx, path := range paths {
				if strings.HasPrefix(filepath.Base(path), md5RecFilePrefix) {
					musicInfos[idx].WavChkSum = "MD5="+md5RecHash
				}
			}
		}
	}

	for idx, musicInfo := range musicInfos {
		if musicInfo.WavChkSum == "" {
			Fprintln(os.Stderr, "No checksum information found:", paths[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}


	var wg sync.WaitGroup

	var destDirNames = make([]string, len(musicInfos))
	var destDirErrs = make([]error, len(musicInfos))

	for idx, musicInfo := range musicInfos {
		wg.Add(1)
		go func(idx int, musicInfo *MusicInfo) {
			defer wg.Done()
			destDirNames[idx], destDirErrs[idx] = getAlbumDirName(musicInfo)
 		}(idx, musicInfo)
	}
	wg.Wait()

	for idx, destDirErr := range destDirErrs {
		if destDirErr != nil {
			Fprintln(os.Stderr, destDirErr, paths[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var destFileNames = make([]string, len(musicInfos))
	var destFileErrs = make([]error, len(musicInfos))
	for idx, musicInfo := range musicInfos {
		wg.Add(1)
		go func(idx int, musicInfo *MusicInfo) {
			defer wg.Done()
			destFileNames[idx], destFileErrs[idx] = getTrackFileName(musicInfo)
 		}(idx, musicInfo)
	}
	wg.Wait()

	for idx, destFileErr := range destFileErrs {
		if destFileErr != nil {
			Fprintln(os.Stderr, destFileErr, paths[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}


	var statDests = make([]string, len(paths))
	var statErrs = make([]error, len(paths))
	for idx := range paths {
		wg.Add(1)
		go func(idx int, destDirName, destFileName string) {
			defer wg.Done()
			var destDirPath = filepath.Join(root, "Albums", destDirName, destFileName)
			var _, statErr = os.Stat(destDirPath)
 			statDests[idx], statErrs[idx] = destDirPath, statErr
		}(idx, destDirNames[idx], destFileNames[idx])
	}
	wg.Wait()

	for idx, statErr := range statErrs {
		if statErr == nil {
			Fprintln(os.Stderr, "Music file already in library:", statDests[idx])
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var copyDests = make([]string, len(paths))
	var copyErrs = make([]error, len(paths))
	for idx, statDest := range statDests {
		wg.Add(1)
		go func(idx int, srcFilePath, destFilePath string) {
			defer wg.Done()
			var destDirPath = filepath.Dir(destFilePath)
			if err := os.MkdirAll(destDirPath, os.ModePerm); err != nil {
				copyDests[idx], copyErrs[idx] = "", err
				return
			}
			
			var srcFile, srcErr = os.Open(srcFilePath)
			if srcErr != nil {
				copyDests[idx], copyErrs[idx] = "", srcErr
				return
			}
			defer srcFile.Close()

			var destFile, destErr = os.Create(destFilePath)
			if destErr != nil {
				copyDests[idx], copyErrs[idx] = "", destErr
				return
			}
			defer destFile.Close()

			var destFileExt = filepath.Ext(destFilePath)
			var jsonFilePath = destFilePath[:len(destFilePath)-len(destFileExt)]+".json"
			var jsonFile, jsonFileErr = os.Create(jsonFilePath)
			if jsonFileErr != nil {
				copyDests[idx], copyErrs[idx] = "", jsonFileErr
				return
			}
			defer jsonFile.Close()

			var _, copyErr = io.Copy(destFile, srcFile)
			if copyErr != nil {
				copyDests[idx], copyErrs[idx] = "", copyErr
				return
			}

			var musicInfoJson, musicInfoJsonErr = json.MarshalIndent(musicInfos[idx], "", "\t")
			if musicInfoJsonErr != nil {
				copyDests[idx], copyErrs[idx] = "", musicInfoJsonErr
				return
			}

			var _, jsonFileWriteErr = jsonFile.Write(musicInfoJson)
			if jsonFileWriteErr != nil {
				copyDests[idx], copyErrs[idx] = "", jsonFileWriteErr
				return
			}

			copyDests[idx], copyErrs[idx] = destFile.Name(), nil
		}(idx, paths[idx], statDest)
	}
	wg.Wait()

	for idx, copyDest := range copyDests {
		if copyDest != "" {
			Fprintln(os.Stdout, "Music file added:", copyDest)
		} else {
			Fprintln(os.Stdout, "Error adding file:", paths[idx])
			if copyErrs[idx] != nil {
				Fprintln(os.Stdout, "Error adding file:", copyErrs[idx])
			}
			exit = true
		}
	}

	if exit {
		os.Exit(1)
	}

	var verifyDestErrs = make([]error, len(statDests))
	for idx, statDest := range statDests {
		wg.Add(1)
		go func(idx int, destFilePath string, musicInfo *MusicInfo) {
			defer wg.Done()

			var wavChkSum = strings.SplitN(musicInfo.WavChkSum, "=", 2)
			if len(wavChkSum) < 2 {
				verifyDestErrs[idx] = errors.New("Malformed checksum '"+musicInfo.WavChkSum+"'")
				return
			}

			var wavChkSumHash hash.Hash
			switch wavChkSum[0] {
			case "MD5":
				wavChkSumHash = crypto.MD5.New()
			default:
				verifyDestErrs[idx] = errors.New("Unknown checksum '"+wavChkSum[0]+"'")
				return
			}

			var cmd = RequestExecCmd("avconv", "-i", destFilePath, "-f", "wav", "-")
			defer ReturnExecCmd(cmd)

			var stdout, oerr = cmd.StdoutPipe()
			if oerr != nil {
				verifyDestErrs[idx] = oerr
				return
			}

			var err = cmd.Start()
			if err != nil {
				verifyDestErrs[idx] = err
				return
			}

			var copySize, copyErr = io.Copy(wavChkSumHash, stdout)
			if copyErr != nil {
				verifyDestErrs[idx] = copyErr
				return
			}

			Println(copySize)

			var wavChkSumVerify = make([]byte, 0, wavChkSumHash.Size())
			wavChkSumVerify = wavChkSumHash.Sum(wavChkSumVerify)

			Println(wavChkSum[1], hex.EncodeToString(wavChkSumVerify))


		}(idx, statDest, musicInfos[idx])
	}
	wg.Wait()


	os.Exit(0)
}

func getAlbumDirName(mi *MusicInfo) (string, error) {
	if mi.Date == 0 {
		return "", errors.New("No 'Date' information found:")
	}
	if mi.Album == "" {
		return "", errors.New("No 'Ablum' information found:")
	}
	var str = Sprintf("%d-%s", mi.Date, sanitizeName(mi.Album))
	return strings.Trim(str, "_"), nil
}

func getTrackFileName(mi *MusicInfo) (string, error) {
	if mi.DiskNumber == 0 {
		return "", errors.New("No 'Disk Number' information found:")
	}
	if mi.TrackNumber == 0 {
		return "", errors.New("No 'Track Number' information found:")
	}
	if mi.Artist == "" {
		return "", errors.New("No 'Artist' information found:")
	}
	if mi.Title == "" {
		return "", errors.New("No 'Title' information found:")
	}
	var ext, err = GetMusicExt(mi.Type)
	if err != nil {
		return "", errors.New("Invalid file type found:")
	}
	var str = Sprintf("D%02d-T%02d-%s-%s%s", mi.DiskNumber, mi.TrackNumber, sanitizeName(mi.Artist), sanitizeName(mi.Title), ext)
	return strings.Trim(str, "_"), nil
}


var SANITIZE_PUNCT_REGEXP = regexp.MustCompile("[\"'`,.!?]+")

var SANITIZE_SPACE_REGEXP = regexp.MustCompile("[\\-+ ()]+")

func sanitizeName(str string) string {
	str = SANITIZE_PUNCT_REGEXP.ReplaceAllString(str, "")
	str = SANITIZE_SPACE_REGEXP.ReplaceAllString(str, "_")
	return str
}
