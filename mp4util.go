package main

import "os"
import "bufio"
import "errors"
import "regexp"
import "strconv"


var M4A_EXT = ".m4a"

var M4A_SIG_VALUE = []byte{ 'M', '4', 'A', ' ' }

var M4A_SIG_OFFSET = int64(8)

var M4A_INFO_HEADER_REGEXP = regexp.MustCompile("(\\d+)\taudio\talac, ([\\d\\.]+) secs, (\\d+) kbps, (\\d+) Hz") // 1	audio	alac, 23.266 secs, 648 kbps, 44100 Hz

var M4A_INFO_TITLE_REGEXP = regexp.MustCompile("Name: (.+)") //  Name: Her Majesty

var M4A_INFO_ARTIST_REGEXP = regexp.MustCompile("Artist: (.+)") //  Artist: The Beatles

var M4A_INFO_DATE_REGEXP = regexp.MustCompile("Release Date: (\\d+)") //  Release Date: 1969

var M4A_INFO_ALBUM_REGEXP = regexp.MustCompile("Album: (.+)") //  Album: Abbey Road

var M4A_INFO_TRACK_REGEXP = regexp.MustCompile("Track: (\\d+) of (\\d+)") //  Track: 17 of 17

var M4A_INFO_DISK_REGEXP = regexp.MustCompile("Disk: (\\d+) of (\\d+)") //  Disk: 1 of 1

var M4A_INFO_GENRE_REGEXP = regexp.MustCompile("Genre: (.+)") //  Genre: Rock

func IsM4aExt(ext string) bool {
	return ext == M4A_EXT
}

func GetM4aExt() string {
	return M4A_EXT
}

func IsM4aFile(file *os.File) (bool, error) {
	sig := make([]byte, len(M4A_SIG_VALUE))
	read, rerr := file.ReadAt(sig, M4A_SIG_OFFSET)
	if rerr != nil {
		return false, rerr
	}
	if read != len(M4A_SIG_VALUE) {
		return false,  nil
	}
	for i, c := range sig {
		if c != M4A_SIG_VALUE[i] {
			return false, nil
		}
	}
	return true, nil
}

func GetM4aInfo(path string) (*MusicInfo, error) {

	var m4aInfo, m4aInfoErr = getM4aInfoFromMp4InfoCmd(path)
	//if m4aInfoErr != nil {
		return m4aInfo, m4aInfoErr
	//}

	//if mp4Info.
}

func getM4aInfoFromMp4InfoCmd(path string) (*MusicInfo, error) {

	var cmd = RequestExecCmd("mp4info", path)
	defer ReturnExecCmd(cmd)

	var stdout, oerr = cmd.StdoutPipe()
	if oerr != nil {
		return nil, oerr
	}

	var err = cmd.Start()
	if err != nil {
		return nil, err
	}
	
	var index = -1
	var minfos = make([]*MusicInfo, 0, 1)
	var lines = bufio.NewScanner(stdout)

	for lines.Scan() {
		var line = lines.Text()
		if m := M4A_INFO_HEADER_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			minfos = append(minfos, &MusicInfo{ Type:"m4a" })
			index++
			continue
		}
		if m := M4A_INFO_TITLE_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				minfos[index].Title = m[1]
			}
			continue
		}
		if m := M4A_INFO_ARTIST_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				minfos[index].Artist = m[1]
			}
			continue
		}
		if m := M4A_INFO_DATE_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				if i, e := strconv.Atoi(m[1]); e == nil {
					minfos[index].Date = i
				}
			}
			continue
		}
		if m := M4A_INFO_ALBUM_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				minfos[index].Album = m[1]
			}
			continue
		}
		if m := M4A_INFO_TRACK_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				if i, e := strconv.Atoi(m[1]); e == nil {
					minfos[index].TrackNumber = i
				}
				if i, e := strconv.Atoi(m[2]); e == nil {
					minfos[index].TrackTotal = i
				}
			}
			continue
		}
		if m := M4A_INFO_DISK_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				if i, e := strconv.Atoi(m[1]); e == nil {
					minfos[index].DiskNumber = i
				}
				if i, e := strconv.Atoi(m[2]); e == nil {
					minfos[index].DiskTotal = i
				}
			}
			continue
		}
		if m := M4A_INFO_GENRE_REGEXP.FindStringSubmatch(line); len(m) > 0 {
			if index >= 0 {
				minfos[index].Genre = m[1]
			}
			continue
		}		
	}

	if len(minfos) == 0 {
		return nil, errors.New("No information found")
	} 

	return minfos[0], nil
}


